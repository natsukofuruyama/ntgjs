/**
 * 引数に与えた角度（度数法）をラジアン（弧度法）にして返します
 *
 * @param {Number} deg
 * @return {Number} element
 */
function degToRad(deg) {
  return deg * Math.PI / 180;
}


/**
 * 引数に与えたラジアン（弧度法）を角度（度数法）にして返します
 *
 * @param {Number}
 * @return {Number} element
 */
function radToDeg(rad) {
  return rad * 180 / Math.PI;
}




var ntgjs = ntgjs || {};
ntgjs.math = ntgjs.math || {};
ntgjs.math = {
  degToRad,
  radToDeg
};